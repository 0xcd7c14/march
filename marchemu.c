#include "march.h"

struct march march_cpu;

int main(int argc, char const *argv[]) {
	if (argc < 2) {
		return 1;
	}
	march_initialize(&march_cpu);
	march_load_rom(&march_cpu, argv[1]);

	while (true) {
		int ret = march_step(&march_cpu);
		if (ret == -1) {
			// end of program
			return 0;
		}
	}

	return 0;
}