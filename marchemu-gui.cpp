// marchemu -- frontend for march emulator

#include <SFML/Graphics.hpp>

#include <string>
#include <vector>
#include <unordered_map>
#include <iostream>
#include <math.h>

#include "march.h"

const int WIDTH = 240;
const int HEIGHT = 160;

std::unordered_map<int, int> sfml_key_interrupt_map = {
	{sf::Keyboard::Left, KEY_LEFT},
	{sf::Keyboard::Right, KEY_RIGHT},
	{sf::Keyboard::Up, KEY_UP},
	{sf::Keyboard::Down, KEY_DOWN},
	{sf::Keyboard::A, KEY_A},
	{sf::Keyboard::B, KEY_B},
	{sf::Keyboard::X, KEY_X},
	{sf::Keyboard::Y, KEY_Y}
};

int main(int argc, char const *argv[]) {

	march march_cpu;

	int timer_msecs;
	bool timer_state;

	srand(time(NULL));

	uint8_t imgdata[WIDTH * HEIGHT * 4];

	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x++) {
			size_t base = (x + WIDTH * y) * 4;
			imgdata[base + 3] = 255;
		}
	}

	if (argc < 2) {
		std::cerr << "usage: march [rom-file]" << std::endl;
		return 1;
	}

	sf::Clock clock;
	
	sf::RenderWindow win (sf::VideoMode(WIDTH, HEIGHT), "march");
	//win.setFramerateLimit(60);

	march_initialize(&march_cpu);
	march_load_rom(&march_cpu, argv[1]);

	sf::Image surf;
	surf.create(WIDTH, HEIGHT, sf::Color::Black);

	sf::Texture surf_texture;
	surf_texture.loadFromImage(surf);

	sf::Sprite surf_sprite;
	surf_sprite.setTexture(surf_texture);

	bool running = true;
	int interrupt_no = -1;

	while (win.isOpen()) {
		sf::Event ev;
		while (win.pollEvent(ev)) {
			if (ev.type == sf::Event::Closed) {
				win.close();
			}
			if (ev.type == sf::Event::KeyReleased) {
				if (ev.key.code == sf::Keyboard::Space) {
					running = !running;
				}
				auto it = sfml_key_interrupt_map.find(ev.key.code);
				if (it != sfml_key_interrupt_map.end()) {
					interrupt_no = it->second;
				} else {
					interrupt_no = -1;
				}
			}
		}
		if (running) {
			int ret;
			if (timer_state && clock.getElapsedTime().asMilliseconds() >= timer_msecs) {
				// timer interrupt
				ret = march_handle_interrupt(&march_cpu, 8);
				timer_msecs = 0;
				timer_state = false;
			}
			ret = march_handle_interrupt(&march_cpu, interrupt_no);
			ret = march_step(&march_cpu);
			if (ret == -1) {
				win.close();
				return 0;
			}
			interrupt_no = -1;
			if (march_cpu.draw_flag) {
				for (int y = 0; y < HEIGHT; y++) {
					for (int x = 0; x < WIDTH; x++) {
						size_t base = (x + WIDTH * y) * 4;
						uint8_t paletteno = march_cpu.screen[y][x];
						uint32_t color = march_cpu.palette[paletteno];
						imgdata[base + 0] = color >> 24;
						imgdata[base + 1] = color >> 16;
						imgdata[base + 2] = color >> 8;
					}
				}
				surf_texture.update(imgdata, WIDTH, HEIGHT, 0, 0);
			}
			march_cpu.draw_flag = 0;
			if (march_cpu.timer != 0) {
				timer_state = true;
				timer_msecs = march_cpu.timer;
				clock.restart();
				march_cpu.timer = 0;
			}
		}
		win.draw(surf_sprite);
		win.display();
	}

	return 0;

}