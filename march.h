// march -- march emulator cpu

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include "font8x8_basic.h"

#define NTOHS(n) (((((unsigned short)(n) & 0xFF)) << 8) | (((unsigned short)(n) & 0xFF00) >> 8))

struct opt_value {
	bool defined;
	int value;
};

struct sprite {
	size_t addr;
	uint8_t w;
	uint8_t h;
};

enum keycode {
	KEY_LEFT,
	KEY_RIGHT,
	KEY_UP,
	KEY_DOWN,
	KEY_A,
	KEY_B,
	KEY_X,
	KEY_Y
};

int msleep(long msec) {
	struct timespec ts;
	int res;

	if (msec < 0) {
		errno = EINVAL;
		return -1;
	}

	ts.tv_sec = msec / 1000;
	ts.tv_nsec = (msec % 1000) * 1000000;

	do {
		res = nanosleep(&ts, &ts);
	} while (res && errno == EINTR);

	return res;
}

struct march {
	
	/* 8K memory */
	uint16_t memory[32768];
	
	/* 32 general purpose registers */
	uint16_t reg[32];
	
	/* program counter */
	uint16_t pc;

	bool pc_set_flag;

	uint16_t opcode;
	
	/* 240x160 color screen */
	uint8_t screen[160][240];

	/* 16 32-bit colors */
	uint32_t palette[16];

	/* 32 sprites */
	struct sprite sprites[32];

	struct opt_value interrupt_table[16];
	
	/* 32 level stack for jumping and subroutines */
	uint16_t stack[32];
	
	/* stack pointer */
	uint16_t sp;
	
	bool draw_flag;

	bool exec_stopped;
	bool exec_paused;

	float timer;

};

void march_initialize(struct march * march) {

	march->pc = 0x0000;
	march->opcode = 0;
	march->sp = 0;

	memset(march->memory, 0, sizeof(march->memory));
	memset(march->stack, 0, sizeof(march->stack));
	memset(march->screen, 0, sizeof(march->screen));
	memset(march->reg, 0, sizeof(march->reg));
	memset(march->palette, 0, sizeof(march->palette));
	memset(march->sprites, 0, sizeof(march->sprites));
	memset(march->interrupt_table, 0, sizeof(march->interrupt_table));

	march->draw_flag = false;
	march->exec_stopped = false;
	march->exec_paused = false;
	march->pc_set_flag = false;

}

void march_load_rom(struct march * march, const char * rom_fpath) {

	unsigned char rom [32768];

	FILE * rom_f = fopen(rom_fpath, "rb");
	if (rom_f != NULL) {
		fseek(rom_f, 0, SEEK_END); 
		int size = ftell(rom_f);
		fseek(rom_f, 0, SEEK_SET);
		fread(rom, size, 1, rom_f);
		fclose(rom_f);
		for (int i = 0; i < size; ++i) {
			march->memory[i] = rom[i];
		}
	}

}

void march_render_char(struct march * march, char c, uint8_t x, uint8_t y, uint8_t fg, uint8_t bg) {
	int set;
	for (int fx = 0; fx < 8; fx++) {
		for (int fy = 0; fy < 8; fy++) {
			set = font8x8_basic[(int)c][fx] & 1 << fy;
			if (set && fg <= 15) {
				march->screen[y+fx][x+fy] = fg;
			}
			if (! set && bg <= 15) {
				march->screen[y+fx][x+fy] = bg;
			}
		}
	}
}

void march_set_pc(struct march * march, int new_pc) {
	march->pc = new_pc;
	march->pc_set_flag = true;
}

int march_step(struct march * march) {

	if (march->exec_stopped) {
		return -1;
	}

	if (march->exec_paused) {
		return 1;
	}

	if (march->pc >= 32768) {
		return -1;
	}

	uint16_t opcode = march->memory[march->pc] << 8 | march->memory[march->pc + 1];
	march->opcode = opcode;
	uint16_t arg1 = march->memory[march->pc + 2] << 8 | march->memory[march->pc + 3];
	uint16_t arg2 = march->memory[march->pc + 4] << 8 | march->memory[march->pc + 5];

	switch (opcode) {

	case 0x0000: {
		break;
	}

	case 0xffff: {
		march->exec_stopped = true;
		return -1;
		break;
	}

	case 0xff00: {
		return 1;
		break;
	}

	case 0x1000: {
		// set palette
		uint16_t P = march->reg[arg1 >> 8];
		uint16_t R = march->reg[arg1 & 0x00ff];
		uint16_t G = march->reg[arg2 >> 8];
		uint16_t B = march->reg[arg2 & 0x00ff];
		march->palette[P] = R << 24 | G << 16 | B << 8 | 255;
		break;
	}

	case 0x1001: {
		// set pixel (X, Y) = Z
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[arg2] <= 15) {
			march->screen[march->reg[Y]][march->reg[X]] = march->reg[arg2];
		}
		break;
	}
	
	case 0x1002: {
		// clear screen
		if (march->reg[arg1] <= 15) {
			for (int y = 0; y < 160; y++) {
				for (int x = 0; x < 240; x++) {
					march->screen[y][x] = march->reg[arg1];
				}
			}
		}
		break;
	}

	case 0x1004: {
		march->draw_flag = true;
		break;
	}

	case 0x1005: {
		// set graphics region
		uint8_t x = march->reg[(arg1 & 0xf000) >> 12];
		uint8_t y = march->reg[(arg1 & 0x0f00) >> 8];
		uint8_t w = march->reg[(arg1 & 0x00f0) >> 4];
		uint8_t h = march->reg[(arg1 & 0x000f)];
		size_t start = march->reg[(arg2 & 0x00ff)];
		size_t i = 0;
		for (int dy = 0; dy <= h; dy++) {
			for (int dx = 0; dx <= w; dx++) {
				if (march->memory[start+i] <= 15) {
					march->screen[y + dy][x + dx] = march->memory[start+i];
				}
				i++;
			}
		}
		break;
	}

	case 0x1006: {
		// register sprite
		// 0xNNSS 0xWWHH
		uint8_t n = march->reg[arg1 >> 8];
		size_t s = march->reg[arg1 & 0x00ff];
		uint8_t w = march->reg[arg2 >> 8];
		uint8_t h = march->reg[arg2 & 0x00ff];
		march->sprites[n].addr = s;
		march->sprites[n].w = w;
		march->sprites[n].h = h;
		break;
	}

	case 0x1007: {
		// draw sprite
		// 0xXXYY 0x00NN
		uint8_t x = march->reg[arg1 >> 8];
		uint8_t y = march->reg[arg1 & 0x00ff];
		uint8_t n = march->reg[arg2 & 0x00ff];
		uint8_t w = march->sprites[n].w;
		uint8_t h = march->sprites[n].h;
		size_t start = march->sprites[n].addr;
		size_t i = 0;
		for (int dy = 0; dy < h; dy++) {
			for (int dx = 0; dx < w; dx++) {
				if (march->memory[start+i] <= 15) {
					march->screen[y + dy][x + dx] = march->memory[start+i];
				}
				i++;
			}
		}
		break;
	}

	case 0x1008: {
		// draw char
		uint8_t x = march->reg[(arg1 & 0xf000) >> 12];
		uint8_t y = march->reg[(arg1 & 0x0f00) >> 8];
		uint8_t fg = march->reg[(arg1 & 0x00f0) >> 4];
		uint8_t bg = march->reg[(arg1 & 0x000f)];
		char ch = march->reg[(arg2 & 0x00ff)];
		march_render_char(march, ch, x, y, fg, bg);
		break;
	}

	case 0x1009: {
		// draw ascii string from memory location
		uint8_t x = march->reg[(arg1 & 0xf000) >> 12];
		uint8_t y = march->reg[(arg1 & 0x0f00) >> 8];
		uint8_t fg = march->reg[(arg1 & 0x00f0) >> 4];
		uint8_t bg = march->reg[(arg1 & 0x000f)];
		size_t memstart = march->reg[arg2 >> 8];
		size_t memend = march->reg[arg2 & 0x00ff];
		uint8_t dx = 0;
		for (size_t i = memstart; i < memend - 1; i++) {
			march_render_char(march, march->memory[i], x+dx, y, fg, bg);
			dx += 8;
		}
		break;
	}

	case 0x2000: {
		// jump to X
		march_set_pc(march, march->reg[arg1]);
		break;
	}

	case 0x2001: {
		// jump to Z if X == Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] == march->reg[Y]) {
			march_set_pc(march, march->reg[arg2]);
		}
		break;
	}

	case 0x2002: {
		// jump to Z if X != Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] != march->reg[Y]) {
			march_set_pc(march, march->reg[arg2]);
		}
		break;
	}

	case 0x2003: {
		// jump to Z if X > Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] > march->reg[Y]) {
			march_set_pc(march, march->reg[arg2]);
		}
		break;
	}

	case 0x2004: {
		// jump to Z if X < Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] != march->reg[Y]) {
			march_set_pc(march, march->reg[arg2]);
		}
		break;
	}

	case 0x2005: {
		// jump to Z if X >= Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] != march->reg[Y]) {
			march_set_pc(march, march->reg[arg2]);
		}
		break;
	}

	case 0x2006: {
		// jump to Z if X <= Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] != march->reg[Y]) {
			march_set_pc(march, march->reg[arg2]);
		}
		break;
	}

	case 0x3001: {
		// call subroutine at X
		march->stack[march->sp] = march->pc;
		march->sp++;
		march_set_pc(march, march->reg[arg1]);
		break;
	}

	case 0x3002: {
		// return from subroutine
		march->sp--;
		if (march->sp > 31) {
			printf("stack underflow, setting to 0...\n");
			march->sp = 0;
		}
		march_set_pc(march, march->stack[march->sp] + 6);
		break;
	}

	case 0x4001: {
		// skip N instructions if X == Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] == march->reg[Y]) {
			march_set_pc(march, march->pc + (6 * (arg2+1)));
		}
		break;
	}

	case 0x4002: {
		// skip N instructions if X != Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] != march->reg[Y]) {
			march_set_pc(march, march->pc + (6 * (arg2+1)));
		}
		break;
	}

	case 0x4003: {
		// skip N instructions if X > Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] > march->reg[Y]) {
			march_set_pc(march, march->pc + (6 * (arg2+1)));
		}
		break;
	}

	case 0x4004: {
		// skip N instructions if X < Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] < march->reg[Y]) {
			march_set_pc(march, march->pc + (6 * (arg2+1)));
		}
		break;
	}

	case 0x4005: {
		// skip N instructions if X >= Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] >= march->reg[Y]) {
			march_set_pc(march, march->pc + (6 * (arg2+1)));
		}
		break;
	}

	case 0x4006: {
		// skip N instructions if X <= Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] <= march->reg[Y]) {
			march_set_pc(march, march->pc + (6 * (arg2+1)));
		}
		break;
	}

	case 0x5000: {
		// compare X and Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		if (march->reg[X] == march->reg[Y]) {
			march->reg[arg2] = 0;
		}
		else if (march->reg[X] < march->reg[Y]) {
			march->reg[arg2] = 1;
		}
		else {
			march->reg[arg2] = 2;
		}
		break;
	}

	case 0x6001: {
		// Z = X + Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		march->reg[arg2] = march->reg[X] + march->reg[Y];
		break;
	}

	case 0x6002: {
		// Z = X - Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		march->reg[arg2] = march->reg[X] - march->reg[Y];
		break;
	}

	case 0x6003: {
		// Z = X * Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		march->reg[arg2] = march->reg[X] * march->reg[Y];
		break;
	}

	case 0x6004: {
		// Z = X / Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		march->reg[arg2] = march->reg[X] / march->reg[Y];
		break;
	}

	case 0x6005: {
		// Z = X mod Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		march->reg[arg2] = march->reg[X] % march->reg[Y];
		break;
	}

	case 0x6006: {
		// Z = X - Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		march->reg[arg2] = march->reg[X] & march->reg[Y];
		break;
	}

	case 0x6007: {
		// Z = X | Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		march->reg[arg2] = march->reg[X] | march->reg[Y];
		break;
	}

	case 0x6008: {
		// Z = X ^ Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		march->reg[arg2] = march->reg[X] ^ march->reg[Y];
		break;
	}

	case 0x7001: {
		// X = N
		march->reg[arg1] = arg2;
		printf("setting X (0x%04x) to %d\n", arg1, march->reg[arg1]);
		break;
	}

	case 0x7002: {
		// X = Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		march->reg[X] = march->reg[Y];
		break;
	}

	case 0x9100: {
		// X = rand()
		//march->reg[arg1] = 4;
		march->reg[arg1] = rand() % (256 + 1);
		break;
	}

	case 0x0600: {
		// memory[X] = Y
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		march->memory[march->reg[X]] = march->reg[Y];
		break;
	}

	case 0x0601: {
		// X = memory[Y]
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		march->reg[X] = march->memory[march->reg[Y]];
		break;
	}

	case 0x0301: {
		// print X
		printf("reg = %d\n", march->reg[arg1]);
		break;
	}

	case 0x0302: {
		// print memory[X:Y]
		uint16_t X = arg1 >> 8;
		uint16_t Y = arg1 & 0x00ff;
		for (int i = march->reg[X]; i < march->reg[Y] - 1; i++) {
			printf("%c", march->memory[i]);
		}
		break;
	}

	case 0x5733: {
		msleep(march->reg[arg1]);
		break;
	}

	case 0x3375: {
		printf("march_step: timer set\n");
		march->timer = march->reg[arg1];
		break;
	}

	case 0x8000: {
		// register interrupt
		uint16_t interrupt_no = march->reg[arg1 >> 8];
		uint16_t subroutine_addr = march->reg[arg1 & 0x00ff];
		printf("interrupt_no reg %d\n", arg1 >> 8);
		printf("interrupt_no reg value = %d\n", march->reg[arg1 >> 8]);
		printf("defining interrupt number %d\n", interrupt_no);
		march->interrupt_table[interrupt_no].defined = true;
		march->interrupt_table[interrupt_no].value = subroutine_addr;
		break;
	}

	case 0x8001: {
		// unregister interrupt
		uint16_t interrupt_no = arg1 & 0x00ff;
		march->interrupt_table[interrupt_no].defined = false;
		break;
	}

	default: {
		printf("unrecognized opcode 0x%04x at memory address 0x%04x\n", opcode, march->pc);
		break;
	}

	}

	if (! march->pc_set_flag) {
		march->pc += 6;
	}
	march->pc_set_flag = false;

	return 0;

}

int march_handle_interrupt(struct march * march, int interrupt_no) {
	
	if (interrupt_no == -1) {
		return -1;
	}
	
	if (! march->interrupt_table[interrupt_no].defined) {
		printf("undefined interrupt\n");
		return -1;
	}
	
	if (march->pc == march->interrupt_table[interrupt_no].value) {
		printf("at interrupt, repeat ignored\n");
		return -1;
	}
	
	if (march->pc != 0) {
		march->stack[march->sp] = march->pc - 6;
	}
	march->sp++;
	march->pc = march->interrupt_table[interrupt_no].value;
	
	return 0;

}