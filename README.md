# March emulator

![](img/invaders.webm)

[`programs/invaders`](programs/invaders)

An emulator for a fictional game console.

Specification is in [`spec.txt`](./spec.txt).

There are two frontends, `marchemu` and `marchemu-gui`. you need the `gui` one for graphics programming.

There is also an [assembler](#assembler).

## Compiling

Requirements: C compiler, C++ compiler, SFML

```sh
$ make marchemu
$ make marchemu-gui
```

## Running

```sh
$ ./marchemu-gui /path/to/rom
# or marchemu for non-graphical frontend
```

## Example programs

Various example programs are given in `programs/`. See the [assembler](#assembler) section on how to compile them into a ROM.

## Assembler

```sh
$ python3 asm.py /path/to/assembly /path/to/rom

# omit the /path/to/rom to output the rom to stdout
# omit both arguments to read assembly from stdin and output the rom to stdout
```

For example, an assembly containing:

```
setreg(R1, 9)
setreg(R2, 100)
setreg(R3, 0)
add(R1, R2, R3)
printreg(R3)
```

produces this ROM file:

```
0x7001 0x0001 0x0009
0x7001 0x0002 0x0064
0x7001 0x0003 0x0000
0x6001 0x0102 0x0003
0x0301 0x0003 0x0000
```

Refer to `instructions.json` for all the assembler instructions. That JSON file also happens to be how the assembler 
knows about various instructions and their arguments.

## Writing programs

A tutorial on how to write programs for this system.

### What you should know

There are no immediate instructions, except for 0x7001 (store in register). every other opcode operates on registers.
This is done mainly to make the instruction set smaller and make code less hardcoded and more dynamically changeable.

Opcodes are 3 words, i.e. 6 bytes each. the first word is the instruction, and the next two are the arguments.

There are 32 registers, value 0 to 31, which can store 1 word.

There are 32 sprites, and 16 palettes.

Note that everything is redrawn every frame, so drawing something and then changing the palette and then 
drawing something else will change the colors of both.

You can use `%label`s to have named jump positions:

```
R_LOOPADDR = R0
R_LIMIT = R1
R_VAR = R2
R_INC = R3
setreg(R_VAR, 1)
setreg(R_INC, 1)
setreg(R_LIMIT, 10)
setreg(R_LOOPADDR, %loop)

%loop:
printreg(R_VAR)
add(R_VAR, R_INC, R_VAR)
skipifgt(R_VAR, R_LIMIT, 1)
jump(R_LOOPADDR)

stopexec()
```

(The `R_LOOPADDR = ...` etc definitions are only there to make the assembly easier to read. They're not present in the final ROM.)

The `skip*` functions take input in simple number of instructions.

### Simple loop

Using this we can make a basic loop...

```
setreg(R0, 0)
setreg(R1, 1)
setreg(R2, 10)
setreg(R3, %loop)
%loop:
skipifeq(R0, R2, 3)
add(R0, R1, R0)
printreg(R0)
jump(R3)
stopexec()
```

It simply counts from 0 to 10, prints each iteration, and exits.

### Subroutines

Use `callsub(R)` to call a subroutine at address in register R, and `retsub` to return to where it was called.
To be specific, it'll start executing the *next* instruction from wherever it was called.

You can nest subroutine calls a maximum of 32 times, since that's how large the call stack is.

```
setreg(R0, 10)
setreg(R1, %mysub)
printreg(R0)
callsub(R1)
printreg(R0)
stopexec()
# we define the subroutine here, since without calling the program execution won't proceed after stopexec
%mysub:
    setreg(R0, 20)
    retsub()
```

output:

```
10
20
```

### Graphics

There is a 240x160 screen which can draw in full 24-bit RGB, but only 16 different colors at a time.

You can draw ASCII text, sprites or individual pixels.

When you're done drawing, `setdrawflag()` to render it to screen.

#### Palette

There are 16 colors in the palette, 0-15. by default all colors are `rgb(0, 0, 0)`. Use `setpalette` to change them.

```
setreg(R0, 1)
setreg(R1, 0)
setreg(R2, 255)
setreg(R3, 0)
setpalette(R0, R1, R2, R3)
```

Passing a palette number higher than 15 to any drawing functions will be treated as "transparent", i.e. whatever pixel was there before will be preserved.

#### Drawing a pixel

Use `drawpixel` to draw a pixel, i.e. set it to a palette color.

```
# don't forget to setpalette before
setreg(R4, 75)
setreg(R5, 100)
drawpixel(R4, R5, R0)
setdrawflag()
pauseexec()
```

sets pixel at (75, 100) to whatever palette number is in R0.

![](img/drawpixel.png)

`pauseexec` is there to prevent the window from closing immediately after our program finishes rendering.

#### Drawing text

There is an inbuilt 8x8 ASCII bitmap font which you can use to easily draw your own text.

Use `drawchar` to draw a single character, or `drawtext` to draw a string stored in memory.

example program: [`programs/drawchar`](programs/drawchar)

![](img/drawchar.png)

Using `drawtext` needs us to store the string in memory somewhere. We can store it somewhere we know it won't be executed, like after `pauseexec`.

example program: [`programs/drawtext`](programs/drawtext)

![](img/drawtext.png)

(`literalstr` is an assembly convenience function which outputs the string encoded as utf-8)

#### Drawing sprites

Use `regsprite` to load a sprite, and `drawsprite` to draw it.

`regsprite` takes arguments RN, RS, RW, RH, which are registers where we have the sprite number, starting memory address, and width and height of the sprite stored respectively.

`drawsprite` takes arguments RX, RY, RN, which are registers where we have X, Y, sprite number stored respectively.

The sprite should be stored in memory as a uint16_t for every two pixels, for example `0x0102` translates into a pixel = `0x01` and the next pixel = `0x02`.

example program: [`programs/sprite`](programs/sprite)

![](img/sprite.png)

#### Clearing the screen

Use `cls`. takes a single argument, a register containing the palette number to clear with.

example program: [`programs/clearscreen`](programs/clearscreen)

![](img/clearscreen.png)

## Inputs and Interrupts

There are 8 keys: up, down, left, right, A, B, X, Y

Keyboard input and timing is done using interrupts.

| Key   | Interrupt |
|-------|-----------|
| left  | 0         |
| right | 1         |
| up    | 2         |
| down  | 3         |
| A     | 4         |
| B     | 5         |
| X     | 6         |
| Y     | 7         |
| timer | 8         |

You can register and unregister an interrupt to a subroutine address using the `(un)reginterrupt` functions.

example program: [`programs/interrupt`](programs/interrupt)

Prints value of R_VAL (i.e. 1) whenever the left key is pressed.

Note that interrupts are processed/handled *before* regular program flow execution.

See [`programs/movement`](programs/movement) for a program which moves a pixel with arrow keys using interrupts.

![movement](img/movement.webm)

See [`programs/goal`](programs/goal) for a simple game where you control a square, and need to get it into another square to win.

![goal](img/goal.webm)

## Timers

There is a millisecond timer which you can use to schedule subroutines to run after a set time.

Use `settimer` to set the timer delay in milliseconds and `reginterrupt` (with interrupt number 8) to set a subroutine to run when timer hits zero.

See [`programs/invaders`](programs/invaders) for an example program which uses timers.

That covers the basics. be sure to read through `spec.txt`, `instructions.json` and the example programs to learn about every instruction.
There aren't too many, so writing programs is simple and easy to learn.