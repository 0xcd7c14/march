marchemu: marchemu.c march.h
	cc -Wall -Wextra -Werror -ggdb3 marchemu.c -o marchemu

marchemu-gui: marchemu-gui.cpp march.h
	c++ -Wall -Wextra -Werror -ggdb3 -lsfml-system -lsfml-window -lsfml-graphics marchemu-gui.cpp -o marchemu-gui
