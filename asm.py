import array
import json
import io
from pathlib import Path
import sys
import argparse
from typing import TextIO, BinaryIO

with open(Path(__file__).parent / "instructions.json", "r") as f:
    instruction_to_asm = json.load(f)

asm_to_instruction = {}
for instr in instruction_to_asm:
    props = instruction_to_asm[instr]
    asm = props["asm"]
    del props["asm"]
    props["instruction"] = instr
    asm_to_instruction[asm] = props

asm_to_instruction["sub"] = asm_to_instruction["subtract"]
asm_to_instruction["drawtext"] = asm_to_instruction["drawstring"]
asm_to_instruction["cls"] = asm_to_instruction["clearscreen"]
asm_to_instruction["jumpsub"] = asm_to_instruction["callsub"]

class NotAValidIntError(ValueError):
    def __init__(self, argno: int):
        self.argno = argno

class ArgumentTooLargeError(ValueError):
    def __init__(self, argno: int, width: int):
        self.argno = argno
        self.width = width

def try_int(x):
    if isinstance(x, int):
        return x
    return int(x, 0)
    
def parse_asm_arguments(asm: str):
    args = []
    in_str = False
    buf = ""
    parens = 1
    for char in asm:
        if char == '"':
            in_str = not in_str
            continue
        if char in {',', ')'} and not in_str:
            args.append(buf)
            buf = ""
            if char == ")":
                break
            continue
        if char == " " and not in_str:
            continue
        buf += char
    return args

def parse_argument_format(arg_format: list[str]) -> tuple[tuple[str, int], ...]:
    args = []
    widths = []
    for word in arg_format:
        word = word.replace("0x", "")
        for char in word:
            if char not in args:
                args.append(char)
                widths.append(word.count(char))
    return tuple(zip(args, widths))

def pack_arguments(arg_format: list[str], args: list[str|int]):
    ref_args = parse_argument_format(arg_format)
    pack = []
    arg_hex = ""
    index_offset = 0
    for i, ref_arg in enumerate(ref_args):
        width = ref_arg[1]
        if ref_arg[0] == '0':
            arg_hex += '0'*width
            index_offset += 1
            continue
        try:
            arg = try_int(args[i-index_offset])
        except ValueError:
            raise NotAValidIntError(i-index_offset)
        if width == 1 and arg > 15:
            # can't be packed
            raise ArgumentTooLargeError(i-index_offset, width)
        arg_hex += f'{arg:0{width}X}'
        if len(arg_hex) == 4:
            pack.append(int(arg_hex, 16))
            arg_hex = ""
    while len(pack) < 2:
        pack.append(0)
    return pack

def literal(args: list[str|int]) -> list[int]:
    return [try_int(x) for x in args]

def literal_size(args: list[str|int]) -> int:
    return  len(array.array('H', literal(args)).tobytes())

def output_BE(vals: list[int], output_f: io.BytesIO):
    vals_bytearray = array.array('H', vals)
    if sys.byteorder == "little":
        vals_bytearray.byteswap()
    vals_bytearray.tofile(output_f)

def print_instruction_docs(instr):
    arg_format = asm_to_instruction[instr]["arg-format"]
    args = (x[0] for x in parse_argument_format(arg_format) if x[0] != '0')
    print(f"Arguments for {instr}:")
    print(" "*4 + ", ".join(args) + " (" + " ".join(arg_format) + ")")
    print(" "*4 + asm_to_instruction[instr]["description"])

def assemble(source_f: TextIO, output_f: BinaryIO):
    definitions = {}
    for i in range(32):
        definitions[f"R{i}"] = i
    labels = {}
    asm = []
    instrnum = 0
    for lineno, line in enumerate(source_f):
        line = line.strip()
        if (not line) or line.startswith("#"):
            continue
        lsplit = line.split()
        if len(lsplit) == 3 and lsplit[1] == "=":
            definitions[lsplit[0]] = definitions.get(lsplit[2], lsplit[2])
        elif line.startswith("%"):
            labels[line.replace(":", "")] = instrnum
        else:
            lfunc = line.split("(", 1)
            func = lfunc[0]
            if func == "literals":
                lfunc[1] = lfunc[1].replace("[", "").replace("]", "")
            args = parse_asm_arguments(lfunc[1])
            asm.append((func, args, lineno+1))
            if func == "literal" or func == "literals":
                instrnum += literal_size(args)
            elif func == "literalstr":
                instrnum += len(args[0].encode('utf-8'))
            else:
                instrnum += 6

    for asm_line in asm:
        func = asm_line[0]
        args = asm_line[1]
        lineno = asm_line[2]
        if func == "literalstr":
            output_f.write(args[0].encode('utf-8'))
            continue
        elif func == "literal" or func == "literals":
            output_BE(literal(args), output_f)
            continue
        instruction = [int(asm_to_instruction[func]["instruction"], 0)]
        args = [labels.get(arg, definitions.get(arg, arg)) for arg in args]
        
        try:
            instruction.extend(pack_arguments(asm_to_instruction[func]["arg-format"], args))
        except IndexError:
            print(f"Line {lineno}: Not enough arguments for {func}.")
            print_instruction_docs(func)
            sys.exit(1)
        except ArgumentTooLargeError as e:
            print(f"Line {lineno}: Argument {args[e.argno]} cannot be packed into field of width {e.width}.")
            print_instruction_docs(func)
            print("Consider using a register <= 15.")
            sys.exit(1)
        except NotAValidIntError as e:
            print(f"Line {lineno}: Argument {args[e.argno]} cannot be converted to an integer.")
            sys.exit(1)
        
        output_BE(instruction, output_f)

def main(argv=[__name__]):
    parser = argparse.ArgumentParser()
    parser.add_argument("asm_file", metavar="asm-file", nargs="?")
    parser.add_argument("output_file", metavar="output-file", nargs="?")
    args = parser.parse_args(argv[1:])
    asm_f = sys.stdin
    out_f = sys.stdout.buffer
    if args.asm_file:
        asm_f = open(Path(args.asm_file), "r")
    if args.output_file:
        out_f = open(Path(args.output_file), "wb")
    assemble(asm_f, out_f)

if __name__ == "__main__":
    main(sys.argv)